package com.example.demo.repository;

import com.example.demo.entity.Taco;

public interface TacoRepository {
	Taco save(Taco design);
}
