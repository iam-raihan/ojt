package com.example.demo.repository;

import com.example.demo.entity.Order;

public interface OrderRepository {
	Order save(Order order);
}
